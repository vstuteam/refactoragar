package bridge;

import java.awt.*;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public interface DishViewImplementation {
    void render(Graphics2D where);
    void setToCenter(GameObjectImplementation imp);
    double getX();
    double getY();
}
