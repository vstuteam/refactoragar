package bridge;

import model.dishobjects.GameObject;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface GameObjectViewImplementation {
    void render(Graphics2D where);
    void setImage(BufferedImage img);
    GameObject getObject();
}
