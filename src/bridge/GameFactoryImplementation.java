package bridge;

import model.dishobjects.GameObject;
import model.view.DishView;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface GameFactoryImplementation {
    GameObjectImplementation createObject(GameObject object);
    GameObjectViewImplementation createView(GameObject object);
    DishViewImplementation createDishView();
    CollisionManagerImplementation createCollisionManager();
    GameImplementation createGame();
    Inputs createInputs();
}
