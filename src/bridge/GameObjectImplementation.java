package bridge;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface GameObjectImplementation extends Collidable {
    double getHorizontalSpeed();
    double getVerticalSpeed();
    void setHorizontalSpeed(double speed);
    void setVerticalSpeed(double speed);
    void setSize(double size);
    void setX(double x);
    void setY(double y);
    void stop();
    void update(long l);
}
