package bridge;

import model.dishobjects.GameObject;

import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface CollisionManagerImplementation {
    void addObject(Collidable object);
    void removeObject(Collidable object);
    ArrayList<ArrayList<GameObject>> update();
}
