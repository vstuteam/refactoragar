package bridge;

import java.awt.*;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface Inputs {
    Point mouseCoordinates();
    boolean mouseClicked();
    boolean keyPressed(java.awt.event.KeyEvent e);
}
