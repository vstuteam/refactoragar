package bridge;

import java.awt.*;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface GameImplementation {
    void updateModel(long l);
    void updateGUI(Graphics2D graphics2D);
    void start();
    void pause();
    void play();
}
