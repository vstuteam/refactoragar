package bridge;

import model.dishobjects.GameObject;

/**
 * Created by Penskoy Nikita on 17.05.2016.
 */
public interface Collidable {
    double getX();
    double getY();
    double getSize();
    GameObject getObject();
}
