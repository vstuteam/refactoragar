package realization;

import bridge.*;
import model.dishobjects.GameObject;
import model.view.DishView;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class GameFactoryGTGE implements GameFactoryImplementation {

    private BackgroundGTGE backgroundGTGE;
    private CollisionManagerGTGE collisionManagerGTGE;
    private GameGTGE gameGTGE;

    @Override
    public GameObjectImplementation createObject(GameObject object) {
        return new GameObjectGTGE(object);
    }

    @Override
    public GameObjectViewImplementation createView(GameObject object) {
        return (GameObjectGTGE)object.getImplementation();
    }

    @Override
    public DishViewImplementation createDishView() {
        if(backgroundGTGE == null)
            backgroundGTGE = new BackgroundGTGE();
        return backgroundGTGE;
    }

    @Override
    public CollisionManagerImplementation createCollisionManager() {
        if(collisionManagerGTGE == null)
            collisionManagerGTGE = new CollisionManagerGTGE();
        return collisionManagerGTGE;
    }

    @Override
    public GameImplementation createGame() {
        if(gameGTGE == null)
            gameGTGE = new GameGTGE();
        return gameGTGE;
    }

    @Override
    public Inputs createInputs() {
        if(gameGTGE == null)
            gameGTGE = new GameGTGE();
        return gameGTGE;
    }
}
