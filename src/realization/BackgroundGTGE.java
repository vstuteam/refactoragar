package realization;

import bridge.DishViewImplementation;
import bridge.GameObjectImplementation;
import bridge.GameObjectViewImplementation;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.background.ImageBackground;
import model.agariospec.Launcher;
import model.dishobjects.GameObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class BackgroundGTGE implements DishViewImplementation {

    private Background bg;

    public BackgroundGTGE() {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("resources/background.jpg"));
        } catch (IOException e) {
        }
        bg = new ImageBackground(img);
        bg.setClip(0, 0, Launcher.screenWidth, Launcher.screenHeight);
    }

    public Background background() {
        return bg;
    }

    @Override
    public void render(Graphics2D where) {
        bg.render(where);
    }

    @Override
    public void setToCenter(GameObjectImplementation imp) {
        bg.setToCenter((GameObjectGTGE)imp);
    }

    @Override
    public double getX() {
        return bg.getX();
    }

    @Override
    public double getY() {
        return bg.getY();
    }
}
