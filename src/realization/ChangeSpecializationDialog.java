package realization;

import model.specialization.Specialization;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class ChangeSpecializationDialog {
    public int showDialog(ArrayList<Specialization> available) {
        int n;
        do {
            // Сфрпмировать список названий доступных специализаций
            Object[] options = new Object[available.size()];
            for (int i = 0; i < available.size(); ++i) {
                options[i] = available.get(i).getName();
            }

            // Запросить у пользователя новую специализацию
            n = JOptionPane.showOptionDialog(null, "Выберите новую специализацию.", "Выбор специализации.",
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        } while (n == JOptionPane.CLOSED_OPTION);
        return n;
    }
}
