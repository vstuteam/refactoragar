package realization;

import bridge.Collidable;
import bridge.CollisionManagerImplementation;
import bridge.GameObjectImplementation;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.collision.BasicCollisionGroup;
import model.agariospec.Factory;
import model.dishobjects.GameObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class CollisionManagerGTGE extends BasicCollisionGroup implements CollisionManagerImplementation {

    ArrayList<ArrayList<GameObject>> collided;
    SpriteGroup group;

    public CollisionManagerGTGE() {
        collided = new ArrayList<>();
        group = new SpriteGroup("Objects");
        setCollisionGroup(group, group);
        pixelPerfectCollision = true;
        group.setBackground(((BackgroundGTGE)Factory.instance().createDishView()).background());
    }

    @Override
    public void collided(Sprite sprite, Sprite sprite1) {
        ArrayList<GameObject> col = new ArrayList<>();
        col.add(((Collidable)sprite).getObject());
        col.add(((Collidable)sprite1).getObject());
        collided.add(col);
    }

    @Override
    public void addObject(Collidable object) {
        group.add((Sprite)object);
    }

    @Override
    public void removeObject(Collidable object) {
        group.remove((Sprite)object);
    }

    @Override
    public ArrayList<ArrayList<GameObject>> update() {
        checkCollision();
        return collided;
    }
}
