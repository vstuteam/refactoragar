package realization;

import bridge.GameObjectImplementation;
import bridge.GameObjectViewImplementation;
import com.golden.gamedev.object.Sprite;
import model.agariospec.Factory;
import model.dishobjects.GameObject;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class GameObjectGTGE extends Sprite implements GameObjectImplementation, GameObjectViewImplementation {

    private GameObject object;
    private int size;

    public GameObjectGTGE(GameObject object) {
        this.object = object;
        this.setBackground(((BackgroundGTGE)(Factory.instance().createDishView())).background());
    }

    @Override
    public void setSize(double size) {
        this.size = (int) size;
    }

    @Override
    public void stop() {
        setSpeed(0,0);
    }

    @Override
    public void update(long l) {
        super.update(l);
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public GameObject getObject() {
        return object;
    }
}
