package realization;

import bridge.GameImplementation;
import bridge.Inputs;
import com.golden.gamedev.Game;
import model.agariospec.GameModel;
import model.view.GUI;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class GameGTGE  extends Game implements GameImplementation, Inputs {

    GameModel model;
    GUI gui;

    @Override
    public void initResources() {
        gui = new GUI();
        model = new GameModel(gui);
    }

    @Override
    public void update(long l) {
        updateModel(l);
    }

    @Override
    public void render(Graphics2D graphics2D) {
        updateGUI(graphics2D);
    }

    @Override
    public void updateModel(long l) {
        model.update(l);
    }

    @Override
    public void updateGUI(Graphics2D graphics2D) {
        gui.update(graphics2D);
    }

    @Override
    public void pause() {

    }

    @Override
    public void play() {

    }

    @Override
    public Point mouseCoordinates() {
        if(bsInput == null)
            return new Point(0,0);
        return new Point(bsInput.getMouseX(),bsInput.getMouseY());
    }

    @Override
    public boolean mouseClicked() {
        return click();
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        return super.keyPressed(e.getKeyCode());
    }
}
