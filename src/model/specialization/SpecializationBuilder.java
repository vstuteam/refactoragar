package model.specialization;

import model.dishobjects.*;

/**
 * Created by Penskoy Nikita on 19.04.2016.
 */
public class SpecializationBuilder {

    SpecializationTree tree;

    Specialization primitive;
    Specialization plant;

    public void primitive() {
        if(tree != null)
            return;
        primitive = new Specialization("Primitive bacterium", 100);

        primitive.addRation(new Ration(new CarbonDioxide(), Ration.HIGH_EFFICIENCY)
                .addRule(new Agar().getType())
                .addRule(new Water().getType())
                .addRule(new Light().getType())
        );

        primitive.addRation(new Ration(new CarbonDioxide(), Ration.HIGH_EFFICIENCY)
                .addRule(primitive.getName())
        );

        tree = new SpecializationTree(primitive);
    }

    public void plant() {
        if(primitive == null)
            primitive();

        plant = new Specialization("Primitive plant", 150);
        plant.addRation(new Ration(new Oxygen())
                .addRule(new CarbonDioxide().getType())
                .addRule(new Water().getType())
                .addRule(new Light().getType())
        );
        plant.addRation(new Ration(new Oxygen())
                .addRule(primitive.getName())
        );
        tree.addSpecialization(plant,primitive);
    }

    public void carnivorousPlant() {

    }
    public void predator() {

    }
    public SpecializationTree getTree() {
        return tree;
    }
}
