package model.specialization;

import model.dishobjects.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Специализация бактерий и болидов
 */
public class Specialization {

    /*
     * Имя специализации
     */
    private String name;

    /**
     * Рационы, относящиеся к текущей специализации
     */
    ArrayList <Ration> rations;
    
    /**
     * Размер объекта, при котором можно улучшать текущую специализацию
     */
    private int upgradeSize;

    public Specialization(String name, int upgradeSize) {
        rations = new ArrayList<>();
        this.name = name;
        this.upgradeSize = upgradeSize;
    }

    /**
     * Добавить рацион к текущей специализации
     * @param ration добавляемый рацион
     * @return текущий экземпляр класса для повторного добавления элементов
     */
    public Specialization addRation(Ration ration) {
        rations.add(ration);
        return this;
    }
    
    /**
     * Проверить допустимость съедения одного объекта другим
     * @param eaten съедаемый объект
     * @param eater съедающий объект
     * @return признак допустимости съедения
     */
    public boolean canEat(GameObject eaten, GameObject eater) {
        // Если объект можно съесть в каком-нибудь из доступных рационов, то его можно есть
        for (Ration ration : rations) {
            if (ration.canEat(eaten, eater)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Получить выполненный рацион
     * @param eaten массив съеденных объектов
     * @return выполненный рацион или null, если ни один не выполнен
     */
    public Ration completedRation(ArrayList <GameObject> eaten) {
        // Распределить съеденные объекты по типам и количеству по каждому типу
        Map <String, Integer> sortedEaten = new HashMap <>();
        for (GameObject obj : eaten) {
            String type = obj.getType();
            // Если объекты этого типа уже были съедены - прибавить к их количеству единицу, иначе создать новую пару ключ-значениесо значением 1
            int count = 1;
            if (sortedEaten.containsKey(type)) {
                count += sortedEaten.get(type);
            }
            sortedEaten.put(obj.getType(), count);
        }
        
        // Проверить выполнение какого-нибудь рациона
        for (Ration ration : rations) {
            if (ration.rationCompleted(sortedEaten)) {
                return ration;
            }
        }
        return null;
    }

    /**
     * Получить строковое название специализации
     * @return строковое описание специализации
     */
    public String getName() {
        return name;
    }

    public boolean equals(Specialization other) {
        return name.equals(other.getName());
    }

    public int upgradeSize() {
        return upgradeSize;
    }
}
