package model.specialization;

/**
 * Created by Penskoy Nikita on 19.04.2016.
 */
public class SpecializationDirector {
    public SpecializationTree constructTree() {
        SpecializationBuilder builder = new SpecializationBuilder();
        builder.plant();
        return builder.getTree();
    }
}
