package model.specialization;

import java.util.*;

/**
 * Класс дерева специализаций
 */
public class SpecializationTree {

    protected HashMap<Specialization,Specialization> parents;
    protected Specialization initial;
    protected Random rand;

    public SpecializationTree(Specialization initial) {
        this.initial = initial;
        parents = new HashMap<>();
    }

    public SpecializationTree addSpecialization(Specialization specialization, Specialization parent) {
        parents.put(specialization,parent);
        return this;
    }

    public Specialization getRandomSpecialization() {
        ArrayList<Specialization> all= new ArrayList<>(Arrays.asList((Specialization[]) parents.keySet().toArray()));
        return all.get(rand.nextInt(all.size()));
    }

    public Specialization getInitialSpecialization() {
        return initial;
    }

    public ArrayList<Specialization> getAllChilds (Specialization parent) {
        ArrayList<Specialization> ans = new ArrayList<>();

        Queue<Specialization> q = new ArrayDeque<>();

        q.add(parent);

        while(!q.isEmpty()) {
            Specialization cur = q.poll();

            ans.add(cur);

            ArrayList<Specialization> childs = getAvailableUpgrades(cur);

            if(childs != null)
            {
                for(Specialization s : childs)
                    q.add(s);
            }
        }

        return ans;
    }

    public boolean isAncestor(Specialization specialization, Specialization ancestor) {
        ArrayList<Specialization> childs = getAllChilds(ancestor);
        return childs.contains(specialization);
    }

    public Specialization getParent(Specialization specialization) {
        return parents.get(specialization);
    }

    public ArrayList<Specialization> getAvailableUpgrades(Specialization specialization) {
        ArrayList<Specialization> childs = new ArrayList<>();
        for(Map.Entry<Specialization, Specialization> entry : parents.entrySet()) {
            if(entry.getValue().equals(specialization)) {
                childs.add(entry.getKey());
            }
        }
        return childs;
    }

    public boolean canUpgrade(Specialization old, Specialization update) {
        return getParent(update).equals(old);
    }

}
