package model.specialization;

import model.dishobjects.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Класс описывает один тип рациона для специализации объектов
 */
public class Ration {

    public static final double HIGH_EFFICIENCY = 1.0;
    public static final double AVERAGE_EFFICIENCY = 0.7;
    public static final double LOW_EFFICIENCY = 0.4;

    private static final int EATEN_SIZE_PER_JUNK = 50;
    private static final int SIZE_PER_GROWTH = 20;

    /**
     * Содержимое рациона, ключ - тип объекта, значение - законы съедения объектов данного типа
     */
    private Map <String, RationRule> ration;

    /**
     * Эффективность роста при выполнении рациона
     */
    private double efficiency;

    /**
     * Результат жизнедеятельности при выполнении рациона
     */
    private PrimitiveObject junk;

    /**
     * Класс, описывающий правила поедания конкретного типа объектов
     */
    private class RationRule {
        
        /**
         * Допустимое соотношение размеров
         */
        public double sizeRatio;
        
        /**
         * Необходимое количество объектов
         */
        public int count;

        /*
         * Поедать ли производные специализации
         */
        public boolean childSpecializations;
        
        public RationRule(int count, boolean childSpecializations, double sizeRatio) {
            this.count = count;
            this.childSpecializations = childSpecializations;
            this.sizeRatio = sizeRatio;
        }

        public RationRule(int count, boolean childSpecializations) {
            this(count, childSpecializations, 1);
        }
        
        public RationRule(int count) {
            this(count, true);
        }
        
        public RationRule() {
            this(1);
        }
    }
    
    public Ration(PrimitiveObject junk, double efficiency) {
        this.junk = junk.clone();
        this.efficiency = efficiency;
        this.ration = new HashMap <String, RationRule>();
    }

    public Ration(PrimitiveObject junk) {
        this(junk, AVERAGE_EFFICIENCY);
    }
    
    /**
     * Добавить правило в рацион
     * @param type тип допустимого объекта
     * @param count количество данного объекта, необходимое для выполнения рациона
     * @param childSpecializations съедать ли дочерние специализации
     * @param sizeRatio коэффициент допустимого для съедения размера
     * @return текущий экземпляр класса для повторного добавления элементов
     */
    public Ration addRule(String type, int count, boolean childSpecializations, double sizeRatio) {
        ration.put(type, new RationRule(count, childSpecializations, sizeRatio));
        return this;
    }

    /**
     * Добавить правило в рацион
     * @param type тип допустимого объекта
     * @param count количество данного объекта, необходимое для выполнения рациона
     * @param childSpecializations съедать ли дочерние специализации
     * @return текущий экземпляр класса для повторного добавления элементов
     */
    public Ration addRule(String type, int count, boolean childSpecializations) {
        return addRule(type, count, childSpecializations, 1);
    }

    /**
     * Добавить правило в рацион
     * @param type тип допустимого объекта
     * @param count количество данного объекта, необходимое для выполнения рациона
     * @return текущий экземпляр класса для повторного добавления элементов
     */
    public Ration addRule(String type, int count) {
        return addRule(type, count, true);
    }
    
    /**
     * Добавить правило в рацион
     * @param type тип допустимого объекта
     * @return текущий экземпляр класса для повторного добавления элементов
     */
    public Ration addRule(String type) {
        return addRule(type, 1);
    }
    
    /**
     * Проверить допустимость съедения одного объекта другим
     * @param eaten съедаемый объект
     * @param eater съедающий объект
     * @return признак допустимости съедения
     */
    public boolean canEat(GameObject eaten, GameObject eater) {
        // Если съедаемый объект включен в текущий рацион и удовлетворяет правилам съедения, то его можно съесть
        if (ration.containsKey(eaten.getType()) &&
                eaten.getSize() <= eater.getSize() * ration.get(eaten.getType()).sizeRatio) {
            return true;
        }
        return false;
    }
    
    /**
     * Проверить, выполняется ли текущий рацион при заданных съеденных объектах
     * @param eaten съеденные объекты, ключ - объект, значение - количество съеденных объектов данного типа
     * @return признак выполнения рациона
     */
    public boolean rationCompleted(Map <String, Integer> eaten) {
        Set <String> types = ration.keySet();
        // Проверить наличие нужного количества объектов всех необходимых типов
        for (String curType : types) {
            if (!eaten.containsKey(curType) || eaten.get(curType) < ration.get(curType).count)
                return false;
        }
        return true;
    }

    /**
     * Получить текущий рацион в виде тип-количество
     * @return текущий рацион в виде тип-количество
     */
    public Map <String, Integer> getRation() {
        // Составить набор пар тип - количество объектов данного типа
        Map <String, Integer> result = new HashMap <String, Integer>();
        Set <String> types = ration.keySet();
        for (String type : types) {
            result.put(type, ration.get(type).count);
        }

        return result;
    }
    
    /**
     * Получить количество продуктов жизнедеятельности для выброса при выполнении рациона
     * @param eatenSize размер съеденных объектов по выполненному рациону
     * @return количество испускаемых продуктов жизнедеятельности
     */
    public int junkCount(int eatenSize) {
        return Math.max(eatenSize / EATEN_SIZE_PER_JUNK, 1);
    }
    
    /**
     * Получить прирост размеров для текущего рациона
     * @param eatenSize суммарный размер съеденных элементов, входящих в данный рацион
     * @return величина прироста размера
     */
    public int growSize(int eatenSize) {
        return (int) (eatenSize * efficiency / SIZE_PER_GROWTH);
    }

    public PrimitiveObject junk() {
        return junk.clone();
    }
}
