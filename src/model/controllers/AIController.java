package model.controllers;

import model.agariospec.Dish;
import model.dishobjects.AliveObject;
import model.dishobjects.Bacterium;
import model.dishobjects.GameObject;
import model.specialization.Specialization;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class AIController implements Controller {

    final double CHANGE_DESTINATION_PROBABILITY = 0.015;
    Random rand = new Random();

    @Override
    public void control(Dish dish, AliveObject bacterium) {
        if (Math.random() < CHANGE_DESTINATION_PROBABILITY) {
            // Сгенерировать случайные новые координаты перемещения
            bacterium.setDestination(new Point(rand.nextInt(Dish.fieldWidth), rand.nextInt(Dish.fieldHeight)));
        }
    }

    @Override
    public void upgradeSpecialization(ArrayList<Specialization> available, AliveObject bacterium) {
        if(available.isEmpty())
            return;
        bacterium.setSpecialization(available.get(rand.nextInt(available.size())));
    }
}
