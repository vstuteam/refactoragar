package model.controllers;

import model.agariospec.Dish;
import model.dishobjects.AliveObject;
import model.dishobjects.Bacterium;
import model.specialization.Specialization;

import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public interface Controller {
    void control(Dish dish, AliveObject bacterium);
    void upgradeSpecialization(ArrayList<Specialization> available, AliveObject bacterium);
}
