package model.controllers;

import model.agariospec.Dish;
import model.agariospec.Factory;
import model.agariospec.Input;
import model.dishobjects.AliveObject;
import model.dishobjects.Bacterium;
import model.dishobjects.Bolid;
import model.specialization.Specialization;
import realization.ChangeSpecializationDialog;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class PlayerController implements Controller {
    @Override
    public void control(Dish dish, AliveObject bacterium) {
        if(bacterium instanceof Bolid)
            return;

        // Определить координаты мыши в окне
        int mx = Input.instance().mouseCoordinates().x;
        int my = Input.instance().mouseCoordinates().y;

        // Определить положение фона относительно окна
        int bgx = (int) Factory.instance().createDishView().getX();
        int bgy = (int) Factory.instance().createDishView().getY();

        // Определить координаты мыши на поле
        int x = mx + bgx;
        int y = my + bgy;

        // Задать направление движения игрока
        bacterium.setDestination(new Point(x, y));

        if(Input.instance().mouseClicked() && bacterium instanceof Bacterium) {
            ((Bacterium) bacterium).throwBolid();
        }
    }

    @Override
    public void upgradeSpecialization(ArrayList<Specialization> available, AliveObject bacterium) {
        if(available.isEmpty())
            return;
        bacterium.setSpecialization(available.get(new ChangeSpecializationDialog().showDialog(available)));
    }
}
