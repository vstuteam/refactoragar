package model.view;

import model.dishobjects.GameObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class AliveObjectView extends GameObjectView {
    public AliveObjectView(GameObject object) {
        super(object);
    }

    public Color getColor() {
        return Color.GREEN;
    }

    public void objectChanged() {
        int size = (int) imp.getObject().getSize();
        Color color = getColor();

        BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bi.createGraphics();
        imp.setImage(bi);

        if(getObject().available()) {
            g2d.setColor(getColor());
            g2d.fillOval(0, 0, bi.getWidth(), bi.getHeight());

            // Обозначить периметр
            g2d.setColor(getColor().darker().darker());
            g2d.setStroke(new BasicStroke(3));
            g2d.drawOval(1, 1, size - 2, size - 2);


            try {
                BufferedImage avatar;
                avatar = ImageIO.read(new File("resources/avatars/" + getObject().getType() + ".png"));

                g2d.drawImage(avatar, ((int) (getObject().getSize()) - avatar.getWidth()) / 2, ((int) (getObject().getSize()) - avatar.getHeight()) / 2, null);
            } catch (IOException ex) {
                Logger.getLogger(getObject().getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
