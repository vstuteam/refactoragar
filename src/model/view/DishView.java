package model.view;

import bridge.DishViewImplementation;
import bridge.GameObjectViewImplementation;
import model.agariospec.Factory;
import model.dishobjects.GameObject;

import java.awt.*;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class DishView {
    protected DishViewImplementation imp;

    public DishView() {
        imp = Factory.instance().createDishView();
    }

    public void update(Graphics2D graphics2D) {
        imp.render(graphics2D);
    }

    public void setToCenter(GameObjectView object) {
        imp.setToCenter(object.getObject().getImplementation());
    }
}
