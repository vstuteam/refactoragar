package model.view;

import bridge.GameObjectViewImplementation;
import model.agariospec.Factory;
import model.dishobjects.GameObject;
import model.dishobjects.PrimitiveObject;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class GUI {

    ArrayList<GameObjectView> objects;
    DishView dishView;

    public GUI() {
        objects = new ArrayList<>();
        dishView = new DishView();
    }

    public void update(Graphics2D g) {
        dishView.update(g);
        for(GameObjectView obj : objects) {
            obj.update(g);
        }
        dishView.setToCenter(objects.get(0));
    }

    public void addObject(GameObject object) {
        GameObjectView view = null;
        if(object instanceof PrimitiveObject) {
            view = new PrimitiveObjectView(object);
        } else {
            view = new AliveObjectView(object);
        }
        objects.add(view);
        view.objectChanged();
    }

    public void removeObject(GameObject object) {
        for(GameObjectView obj : objects) {
            if(obj.getObject().equals(object)) {
                objects.remove(obj);
                return;
            }
        }
    }

    public void objectChanged (GameObject object) {
        for(GameObjectView obj : objects) {
            if(obj.getObject().equals(object)) {
                obj.objectChanged();
                return;
            }
        }
    }
}
