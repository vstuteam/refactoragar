package model.view;

import model.dishobjects.*;
import model.specialization.SpecializationTree;

import java.awt.*;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class PrimitiveObjectView extends GameObjectView {
    public PrimitiveObjectView(GameObject object) {
        super(object);
    }

    public Color getColor() {
        if(getObject().getClass() == Agar.class) {
            return Color.LIGHT_GRAY;
        }
        if(getObject().getClass() == CarbonDioxide.class) {
            return Color.BLACK;
        }
        if(getObject().getClass() == Light.class) {
            return Color.YELLOW;
        }
        if(getObject().getClass() == Oxygen.class) {
            return Color.WHITE;
        }
        if(getObject().getClass() == Water.class) {
            return Color.BLUE;
        }
        return super.getColor();
    }
}
