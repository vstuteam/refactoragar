package model.view;

import bridge.GameObjectViewImplementation;
import model.agariospec.Factory;
import model.dishobjects.GameObject;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class GameObjectView {
    protected GameObjectViewImplementation imp;

    public GameObjectView(GameObject object) {
        this.imp = Factory.instance().createView(object);
    }

    GameObject getObject() {
        return imp.getObject();
    }

    public void update(Graphics2D graphics2D) {
        imp.render(graphics2D);
    }

    public Color getColor() {
        return Color.RED;
    }

    public void objectChanged() {
        int size = (int) imp.getObject().getSize();
        Color color = getColor();

        BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bi.createGraphics();
        imp.setImage(bi);

        if(getObject().available()) {
            g2d.setColor(getColor());
            g2d.fillOval(0, 0, bi.getWidth(), bi.getHeight());

            // Обозначить периметр
            g2d.setColor(getColor().darker().darker());
            g2d.setStroke(new BasicStroke(3));
            g2d.drawOval(1, 1, size - 2, size - 2);
        }
    }
}
