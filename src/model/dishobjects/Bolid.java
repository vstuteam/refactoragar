package model.dishobjects;

import model.controllers.Controller;
import model.specialization.Specialization;

import java.awt.*;

/**
 * описывает болид бактерии
 */
public class Bolid extends AliveObject {

    Bacterium parent;
    public Bolid(int size, Specialization spec, Controller controller, Bacterium parent) {
        super(size, spec, controller);
        this.parent = parent;
        setDestination(parent.getDestination());
    }

    @Override
    public void collideWith(GameObject object) {
        if(object != parent)
            eat(object);
        if(object.available() && object instanceof Bacterium) {
            ((Bacterium) object).throwBolid((int) getSize());
            setDestination(new Point((int)(getPosition().getX()*2 - object.getPosition().getX()),
                    (int)(getPosition().getY()*2 - object.getPosition().getY())));
        }
    }

    @Override
    public void update(long l) {
        setDestination(parent.getDestination());
        super.update(l);
    }
}
