package model.dishobjects;

import java.awt.Color;

/**
 * описывает примитивный объект свет
 */
public class Light extends PrimitiveObject {
    
    /**
     * Размеры частиц света по умолчанию
     */
    static int INITIAL_SIZE = 20;

    public Light() {
        this(INITIAL_SIZE);
    }

    public Light(int size) {
        super(size);
    }

    @Override
    public PrimitiveObject clone() {
        return new Light();
    }
}
