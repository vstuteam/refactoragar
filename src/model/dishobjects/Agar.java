package model.dishobjects;

import java.awt.Color;

/**
 * описывает примитивный объект агар
 */
public class Agar extends PrimitiveObject {

    /**
     * Размеры частиц агара по умолчанию
     */
    static int INITIAL_SIZE = 20;

    public Agar() {
        this(INITIAL_SIZE);
    }

    public Agar(int size) {
        super(size);
    }

    @Override
    public PrimitiveObject clone() {
        return new Agar();
    }
}
