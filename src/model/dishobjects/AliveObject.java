package model.dishobjects;

import model.controllers.Controller;
import model.specialization.Ration;
import model.specialization.Specialization;

import java.awt.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Описывает объект, который питается и движется
 */
public abstract class AliveObject extends GameObject {

    /**
     * Коэффициент дальности полёта результата жизнедеятельности от размера текущего объекта
     */
    final double JUNK_DISTANCE_COEFFICIENT = 3;
    
    /**
     * Специализация объекта
     */
    private Specialization specialization;
    
    /**
     * Массив съеденных объектов
     */
    protected ArrayList <GameObject> eaten;

    public Controller getController() {
        return controller;
    }

    private Controller controller;
    
    public AliveObject(int size, Specialization spec, Controller controller) {
        super(size);
        eaten = new ArrayList <GameObject>();
        setSpecialization(spec);
        this.controller = controller;
    }
    
    /**
     * Съесть объект
     * @param obj съедаемый объект
     * @return признак успешного съедения
     */
    public boolean eat(GameObject obj) {
        // Если съедаемый объект не входит в нашу специализацию - не есть его
        if (!specialization.canEat(obj, this))
            return false;
        
        // Добавить в массив съеденных объектов
        eaten.add(obj);
        
        // Удалить съеденный объект с поля
        dish.removeObject(obj);
        
        // Проверить выполнение рациона
        Ration completedRation = specialization.completedRation(eaten);
        
        // Если выполнился какой-то рацион
        if (completedRation != null) {
            int eatenSize = 0;          // суммарные размеры съеденных объектов, входящих в выполненный рацион
            
            // Удалить из массива съеденных объектов объекты рациона
            Map <String, Integer> objects = completedRation.getRation();
            Set <String> types = objects.keySet();
            for (String type : types) {
                for (int i = 0; i < eaten.size(); ++i) {
                    // Если найден съеденный объект нужного типа
                    if (eaten.get(i).getType().equals(type)) {
                        // Увеличить суммарный размер съеденных объектов текущего рациона на размер текущего элемента
                        eatenSize += eaten.get(i).getSize();
                        
                        // Удалить его из съеденных объектов
                        eaten.remove(i);
                        i--;
                        
                        // Уменьшить количество оставшихся востребованных объектов данного типа
                        objects.put(type, objects.get(type) - 1);
                        
                        // Если набрано нужное количетсво объектов текущего типа - перейти кследующему
                        if (objects.get(type) == 0) {
                            break;
                        }
                    }
                }
            }
            
            // Получить количество испускаемых продуктов жизнедеятельности при выполнении текущего рациона
            int junkCount = completedRation.junkCount(eatenSize);
            
            // Увеличиться в размерах
            setSize(getSize() + completedRation.growSize(eatenSize));
                    
            // Испустить продукты жизнедеятельности
            for (int i = 0; i < junkCount; ++i) {
                throwJunk(completedRation.junk());
            }
        }

        if(getSize() >= specialization.upgradeSize()) {
            controller.upgradeSpecialization(dish.specializationTree().getAvailableUpgrades(specialization), this);
        }
        
        return true;
    }

    /**
     * Выплюнуть результат жизнедеятельности в случайном направлении
     * @param junk объект результата жизнедеятельности
     */
    public void throwJunk(PrimitiveObject junk) {
        // Вычислить расстояние, на которое полетит объект
        int distance = (int) (getSize() * JUNK_DISTANCE_COEFFICIENT);

        // Вычислить случайное отклонение по оси Х от текущего объекта
        Random rand = new Random();
        int dx = rand.nextInt(2 * distance) - distance;

        // Вычислить отклонение по оси y, исходя из отклонения по оси x и дистанции по теореме Пифагора
        int dy = (int) Math.sqrt(distance * distance - dx * dx) * (rand.nextInt(2) == 0 ? -1 : 1);

        // Создать объект результата жизнедеятельности
        Point curPosition = getPosition();
        dish.addObject(junk, curPosition);

        // Задать точку назначения полёта
        junk.setDestination(new Point(curPosition.x + dx, curPosition.y + dy));
    }
    
    /**
     * Установить специализацию объекта
     * @param spec устанавливаемая специализация
     */
    public void setSpecialization(Specialization spec) {
        specialization = spec;
        objectChanged();
    }

    public Specialization getSpecialization() {
        return specialization;
    }
    
    @Override
    public String getType() {
        return specialization.getName();
    }

    @Override
    public void update(long l) {
        super.update(l);
        controller.control(dish,this);
    }
}
