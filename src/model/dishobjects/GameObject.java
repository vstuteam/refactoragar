package model.dishobjects;

import bridge.GameObjectImplementation;
import model.agariospec.Dish;
import com.golden.gamedev.object.Sprite;
import model.agariospec.Factory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Описывает объект, хранящийся на поле
 */
public abstract class GameObject {

    private final int STOP_MOVE_RADIUS = 15;
    private final double SPEED_PER_SIZE = 25;
    private final double BREAK_COEFFICIENT = 500;

    private double speed;
    private Point destination;
    private GameObjectImplementation imp;
    private boolean available;

    protected Dish dish;
    
    public GameObject(int size) {
        imp = Factory.instance().createObject(this);
        setSize(size);
        available = true;
    }
    
    /**
     * Получить тип объекта
     * @return тип объекта
     */
    public abstract String getType();

    public GameObjectImplementation getImplementation() {
        return imp;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public boolean available() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public abstract void collideWith(GameObject object);

    public double getHorizontalSpeed() {
        return imp.getHorizontalSpeed();
    }

    public double getVerticalSpeed() {
        return imp.getVerticalSpeed();
    }

    public void setHorizontalSpeed(double speed) {
        imp.setHorizontalSpeed(speed);
    }

    public void setVerticalSpeed(double speed) {
        imp.setVerticalSpeed(speed);
    }

    public void stop() {
        imp.stop();
    }

    public double getX() {
        return imp.getX() + getSize() / 2;
    }

    public double getY() {
        return imp.getY() + getSize() / 2;
    }

    public double getSize() {
        return imp.getSize();
    }
    
    /**
     * Задать направление движения объекта
     * @param destination пункт назначение движения
     */
    public void setDestination(Point destination) {
        this.destination = destination;
    }
    public Point getDestination() {
        return destination;
    }
    
    public void update(long l) {
        // Есои направление перемещения не задано, то не перемещаемся никуда
        if (destination == null)
            return;

        double distance = distance(destination);
        double dx = destination.getX() - getX();
        double dy = destination.getY() - getY();

        // Если цель достигнута
        if (distance <= STOP_MOVE_RADIUS) {
            stop();
            destination = null;
        }
        // Иначе устанавливаем скорости по осям координат
        else {
            setHorizontalSpeed(Math.min(speed, distance / BREAK_COEFFICIENT) * dx / (Math.abs(dx) + Math.abs(dy)));
            setVerticalSpeed(Math.min(speed, distance / BREAK_COEFFICIENT) * dy / (Math.abs(dx) + Math.abs(dy)));
        }

        // Проверить на выход за границы поля
        if (getX() <= 0 && getHorizontalSpeed() < 0)
            setHorizontalSpeed(0);
        if (getX() + getSize() >= Dish.fieldWidth && getHorizontalSpeed() > 0)
            setHorizontalSpeed(0);
        if (getY() <= 0 && getVerticalSpeed() < 0)
            setVerticalSpeed(0);
        if (getY() + getSize() >= Dish.fieldHeight && getVerticalSpeed() > 0)
            setVerticalSpeed(0);

        imp.update(l);
    }

    public double distance(Point o) {
        return o.distance(getX(),getY());
    }

    public double distanceSq(Point o) {
        return o.distanceSq(getX(),getY());
    }
    
    /**
     * Установить новый размер объекта
     * @param size размер объекта
     */
    protected void setSize(double size) {
        imp.setSize(size);
        // Задать скорость относительно нового размера
        speed = SPEED_PER_SIZE / size;
        objectChanged();
    }

    protected void objectChanged() {
        if(dish != null) {
            dish.objectChanged(this);
        }
    }
    
    /**
     * Получить позицию объекта
     * @return позиция объекта
     */
    public Point getPosition() {
        Point position = new Point();
        position.x = (int) (getX() + getSize() / 2);
        position.y = (int) (getY() + getSize() / 2);
        return position;
    }
    
    /**
     * Установить позицию элемента в чашке Петри
     * @param position позиция элемента
     */
    public void setPosition(Point position) {
        imp.setX(position.getX() - getSize() / 2);
        imp.setY(position.getY() - getSize() / 2);
    }
}
