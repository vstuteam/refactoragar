package model.dishobjects;

import model.agariospec.Dish;

/**
 * описывает примитивный объект
 */
public abstract class PrimitiveObject extends GameObject {
    
    public PrimitiveObject(int size) {
        super(size);
    }
    public abstract PrimitiveObject clone();

    public void collideWith(GameObject object) {

    }

    @Override
    public String getType() {
        return getClass().getSimpleName();
    }
}