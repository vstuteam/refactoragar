package model.dishobjects;

import model.controllers.Controller;
import model.specialization.Specialization;

/**
 * Класс описывает движущуюся бактерию
 */
public class Bacterium extends AliveObject {

    /**
     * Сколько болидов максимального размера помещается в бактерию
     */
    final int MAX_BOLID_SIZE_MULTIPLE = 3;

    public Bacterium(int size, Specialization spec, Controller controller) {
        super(size, spec,controller);
    }
    
    /**
     * Выстрелить болид
     */
    public void throwBolid(int size) {
        if(getSize() > size* MAX_BOLID_SIZE_MULTIPLE) {
            dish.addObject(new Bolid(size, getSpecialization(), getController(), this),getPosition());
            setSize(getSize() - size);
        }
    }

    public void throwBolid() {
        throwBolid((int) (getSize()/ MAX_BOLID_SIZE_MULTIPLE));
    }
    
    /**
     * Поглотить дочерний болид
     * @param bolid 
     */
    public void absorb(Bolid bolid) {
        setSize(getSize() + bolid.getSize());
        eaten.addAll(bolid.eaten);
        dish.removeObject(bolid);
    }

    @Override
    public void collideWith(GameObject object) {
        if(object instanceof Bolid) {
            if(((Bolid) object).parent != this)
                return;
            absorb((Bolid) object);
        }
        eat(object);
    }
}
