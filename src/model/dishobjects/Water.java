package model.dishobjects;

import java.awt.Color;

/**
 * описывает примитивный объект воду
 */
public class Water extends PrimitiveObject {
    
    /**
     * Размеры частиц воды по умолчанию
     */
    static int INITIAL_SIZE = 20;

    public Water() {
        this(INITIAL_SIZE);
    }

    public Water(int size) {
        super(size);
    }

    @Override
    public PrimitiveObject clone() {
        return new Water();
    }
}
