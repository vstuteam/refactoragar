package model.dishobjects;

import java.awt.Color;

/**
 * описывает примитивный объект углекислый газ
 */
public class CarbonDioxide extends PrimitiveObject {
    
    /**
     * Размеры частиц углекислого газа по умолчанию
     */
    static int INITIAL_SIZE = 20;

    public CarbonDioxide() {
        this(INITIAL_SIZE);
    }

    public CarbonDioxide(int size) {
        super(size);
    }

    @Override
    public PrimitiveObject clone() {
        return new CarbonDioxide();
    }

}