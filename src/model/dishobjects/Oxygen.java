package model.dishobjects;

import java.awt.Color;

/**
 * описывает примитивный объект кислород
 */
public class Oxygen extends PrimitiveObject {
    
    /**
     * Размеры частиц кислорода по умолчанию
     */
    static int INITIAL_SIZE = 20;

    public Oxygen() {
        this(INITIAL_SIZE);
    }

    public Oxygen(int size) {
        super(size);
    }

    @Override
    public PrimitiveObject clone() {
        return new Oxygen();
    }
}
