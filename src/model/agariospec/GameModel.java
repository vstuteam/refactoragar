package model.agariospec;

import bridge.GameImplementation;
import model.specialization.SpecializationDirector;
import model.view.GUI;

/**
 * Модель игры
 */
public class GameModel {

    private GameImplementation imp;
    private Dish dish;

    public GameModel(GUI gui) {
        imp = null;
        dish = new Dish(new SpecializationDirector().constructTree(), gui);
    }

    public Dish dish() {
        return dish;
    }

    public void startGame() {
        imp.start();
    }

    public void freeze() {
        imp.pause();
    }

    public void unfreeze() {
        imp.play();
    }

    public void update(long l) {
        dish.update(l);
    }
}