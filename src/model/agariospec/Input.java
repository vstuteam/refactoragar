package model.agariospec;

import bridge.Inputs;

import java.awt.*;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class Input {
    private static Input instance;
    private Inputs imp;

    private Input() {
        imp = Factory.instance().createInputs();
    }

    public static Input instance() {
        if(instance == null)
            instance = new Input();
        return instance;
    }

    public Point mouseCoordinates() {
        return imp.mouseCoordinates();
    }

    public boolean mouseClicked() {
        return imp.mouseClicked();
    }

    public boolean keyPressed(java.awt.event.KeyEvent e) {
        return imp.keyPressed(e);
    }
}
