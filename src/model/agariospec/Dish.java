package model.agariospec;

import bridge.Collidable;
import bridge.CollisionManagerImplementation;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.SpriteGroup;
import model.controllers.AIController;
import model.controllers.PlayerController;
import model.dishobjects.*;
import model.specialization.*;
import model.specialization.SpecializationTree;
import model.view.GUI;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 * Чашка петри с объектами (поле)
 */
public class Dish {

    final int OBJECT_ADD_ATTEMPTS_COUNT = 20;
    final int START_BACTERIUM_COUNT = 5;
    final int START_RESOURCE_COUNT = 20;
    final int INITIAL_BACTERIUM_SIZE = 50;
    
    /**
     * Размеры поля
     */    
    public final static int fieldWidth = 3000,
            fieldHeight = 1985;
    
    /**
     * Объекты в чашке
     */
    private ArrayList <GameObject> objects;
    private ArrayList <GameObject> tobe_added_objects;
    boolean updating;
    private SpecializationTree tree;
    private Random rand;
    private CollisionManager collisionManager;
    private GUI gui;

    public Dish(SpecializationTree tree, GUI gui) {
        objects = new ArrayList <>();
        tobe_added_objects = new ArrayList<>();
        updating = false;
        this.tree = tree;
        rand = new Random();
        this.gui = gui;
        collisionManager = new CollisionManager();

        createPlayerBacterium();
        for(int i =0; i< START_BACTERIUM_COUNT; ++i) {
            createBacterium();
        }
        for(int i =0; i< START_RESOURCE_COUNT; ++i) {
            createResource();
        }
    }

    Bacterium createPlayerBacterium() {
        Bacterium player = new Bacterium(INITIAL_BACTERIUM_SIZE, tree.getInitialSpecialization(), new PlayerController());
        addObjectToRandomPosition(player);
        return player;
    }

    Bacterium createBacterium() {
        Bacterium bacterium = new Bacterium(INITIAL_BACTERIUM_SIZE, tree.getInitialSpecialization(), new AIController());
        addObjectToRandomPosition(bacterium);
        return bacterium;
    }

    PrimitiveObject createResource() {
        PrimitiveObject[] resources = {new Agar(), new Light(), new Water()};
        PrimitiveObject resource = resources[rand.nextInt(resources.length)];
        addObjectToRandomPosition(resource);
        return resource;
    }

    public SpecializationTree specializationTree() {
        return tree;
    }

    /**
     * Добавить объект на поле
     * @param obj добавляемый объект
     * @param position позиция добавляемого объекта
     */
    public void addObject(GameObject obj, Point position) {
        obj.setPosition(position);
        obj.setDish(this);

        if(updating) {
            tobe_added_objects.add(obj);
            return;
        }

        objects.add(obj);
        collisionManager.addObject(obj.getImplementation());
        gui.addObject(obj);
    }
    
    /**
     * Добавить объект на поле в случайную свободную позицию
     * @param obj добавляемый объект
     * @return признак успешного добавления объекта
     */
    public boolean addObjectToRandomPosition(GameObject obj) {
        // Предпринять некоторое количество попыток разместить объект
        for (int i = 0; i < OBJECT_ADD_ATTEMPTS_COUNT; ++i) {
            // Сгенерировать случайные координаты для объекта
            Point position = new Point(rand.nextInt(fieldWidth), rand.nextInt(fieldHeight));
            // Попытаться добавить объект по данным координатам
            if (isFreePosition(position, obj)) {
                addObject(obj, position);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Удалить объект с поля
     * @param obj удаляемый объект
     */
    public void removeObject(GameObject obj) {
        if (objects.contains(obj)) {
            // Удалить объект с поля
            obj.setAvailable(false);
            objects.remove(obj);
            collisionManager.removeObject(obj.getImplementation());
            gui.removeObject(obj);
        }
    }
    
    /**
     * Получить объекты на поле
     * @return массив объектов на поле
     */
    public ArrayList<GameObject> getObjects(String type) {
        ArrayList<GameObject> result = new ArrayList<>();
        for (GameObject object : objects) {
            if(object.getType().equals(type))
                result.add(object);
        }
        return result;
    }

    /**
     * Получить объекты на поле
     * @return массив объектов на поле
     */
    public ArrayList<GameObject> getAllObjects() {
        return objects;
    }

    public void objectChanged(GameObject object) {
        gui.objectChanged(object);
    }

    void update(long l) {

        if(rand.nextDouble() < 0.05) {
            createResource();
        }
        if(rand.nextDouble() < 0.02) {
            createBacterium();
        }

        for (GameObject object: tobe_added_objects) {
            addObject(object, object.getPosition());
        }
        tobe_added_objects = new ArrayList<>();

        updating = true;

        collisionManager.update(l);
        for (GameObject object: objects) {
            object.update(l);
        }

        updating = false;
    }
    
    /**
     * Проверить, можно ли в заданной позиции разместить заданный объект без пересечений с остальными
     * @param position позиция, в которую нужно разместить объект
     * @param obj добавляемый объект
     * @return доступность позиции для размещения объекта
     */
    private boolean isFreePosition(Point position, GameObject obj) {
        int x1 = (int) position.getX(), y1 = (int) position.getY();
        
        // Проверить выход за границы поля
        if (x1 - obj.getSize() / 2.0 < 0 || x1 + obj.getSize() / 2.0 >= fieldWidth ||
                y1 - obj.getSize() / 2.0 < 0 || y1 + obj.getSize() / 2.0 >= fieldHeight)
            return false;
        
        for (GameObject curObj : objects) {
            // Координаты центра текущего объекта
            int x2 = (int) curObj.getPosition().getX(), y2 = (int) curObj.getPosition().getY();
            
            // Подсчёт расстояния между центрами центрами объектов по теореме Пифагора
            double distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            
            // Проверка наложения
            if (distance <= (obj.getSize() + curObj.getSize()) / 2.0)
                return false;
        }
        return true;
    }

    class CollisionManager {
        CollisionManagerImplementation imp;

        CollisionManager() {
            imp = Factory.instance().createCollisionManager();
        }

        void collided(ArrayList<GameObject> objects) {
            if(objects == null)
                return;
            for(GameObject object : objects) {
                if(object.available()) {
                    for(GameObject other : objects) {
                        if(!object.equals(other) && other.available()) {
                            object.collideWith(other);
                        }
                    }
                }
            }
        }

        void addObject(Collidable object) {
            imp.addObject(object);
        }

        void removeObject(Collidable object) {
            imp.removeObject(object);
        }

        void update(long l) {
            ArrayList<ArrayList<GameObject>> collided = imp.update();
            for(ArrayList<GameObject> collide : collided) {
                collided(collide);
            }
        }
    }
}
