package model.agariospec;

import bridge.*;
import model.dishobjects.GameObject;
import model.view.DishView;
import realization.GameFactoryGTGE;

/**
 * Created by Penskoy Nikita on 19.05.2016.
 */
public class Factory {
    GameFactoryImplementation imp;

    private static Factory instance;
    private Factory() {
        imp = new GameFactoryGTGE();
    }

    public static Factory instance() {
        if(instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public GameObjectImplementation createObject(GameObject object) {
        return imp.createObject(object);
    }

    public GameObjectViewImplementation createView(GameObject object) {
        return imp.createView(object);
    }

    public DishViewImplementation createDishView() {
        return imp.createDishView();
    }

    public CollisionManagerImplementation createCollisionManager() {
        return imp.createCollisionManager();
    }

    public GameImplementation createGame() {
        return imp.createGame();
    }

    public Inputs createInputs() {
        return imp.createInputs();
    }
}
