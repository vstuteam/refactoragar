package model.agariospec;

import com.golden.gamedev.GameLoader;
import realization.GameGTGE;

import java.awt.Dimension;

/**
 * Главный класс игры
 */
public class Launcher {

    public static int screenWidth = 1024,
        screenHeight = 768;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GameLoader game = new GameLoader();
        game.setup((GameGTGE)Factory.instance().createGame(), new Dimension(screenWidth, screenHeight), false);
        game.start();
    }   
}